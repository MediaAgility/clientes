import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'TPD Enterprise';
  bienvenida: string = 'Bienvenido Josue Sandoval';
  curso = 'Curso Spring 5Angular 7';
  profesor: string = 'Andres Guzman';
}
