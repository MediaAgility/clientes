import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  private cliente: Cliente = new Cliente();
  private titulo: string = "Crear Cliente";
  // Arreglo de errores
  private errors: string[];

  constructor(private clienteService: ClienteService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.cargarCliente();
  }

  cargarCliente(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if (id) {
        this.clienteService.getCliente(id).subscribe((cliente) => this.cliente = cliente)
      }
    })
  }

  public create(): void {
    this.clienteService.create(this.cliente).subscribe(
      response => {
        this.router.navigate(['/clientes'])
        Swal.fire(
          'Cliente Creado!',
          `Cliente ${response.cliente.nombre} creado exitosamente!`,
          'success'
        )
      },
      err => {
        this.errors = err.error.errors as string[];
        // Opcionalmente
        console.error(err.error.errors);
      }
    );
  }

  update(): void {
    this.clienteService.update(this.cliente)
      .subscribe(response => {
        this.router.navigate(['/clientes'])
        Swal.fire(
          'Cliente actualizado!',
          `Cliente ${response.cliente.nombre} actualizado exitosamente!`,
          'success'
        )
      },
        err => {
          this.errors = err.error.errors as string[];
          // Opcionalmente
          console.error(err.error.errors);
        }
      )
  }

}
