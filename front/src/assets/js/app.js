// ESTRUCTURA ENCAPSULADA
(function () {
  var app = angular.module('myApp', []);

  // SE CONOCEN COMO INYECCIONES EL HTTP
  app.controller('mainCtrl', ['$scope', '$http', function ($scope, $http) {

    $('.loader').hide();

    $scope.enviarForm = function (formulario) {

      let calle = $('#idStreet').val();
      let numCalle = $('#idStreetNumber').val();
      let colonia = $('#idNeighborhood').val();
      let municipio = $('#idMunicipality').val();
      let estado = $('#idState').val();
      let cp = $('#idCP').val();
      let referencia = $('#idRef').val();
      let entrega = $('#idAutocomplete').val();
      let idNumInt = $('#idNumInt').val();


      $scope.data =

        {
          "templateType": 6247857719869440,
          "fields": [{
              "name": "jobCode",
              "inputValue": "101859326"
            },
            {
              "name": "ticketNumber",
              "inputValue": "4"
            },
            {
              "name": "customerName",
              "inputValue": formulario.cliente.nombre
            },
            {
              "name": "customerLastName",
              "inputValue": formulario.cliente.apellido
            },
            {
              "name": "dropLocation",
              "inputValue": "19.4271842,-99.1718898"
            },
            {
              "name": "dropAddress",
              "inputValue": "80, rio volga"
            },
            {
              "name": "suiteNumber",
              "inputValue": numCalle
            },
            {
              "name": "streetName",
              "inputValue": calle
            },
            {
              "name": "buildingNumber",
              "inputValue": idNumInt
            },
            {
              "name": "colony",
              "inputValue": colonia
            },
            {
              "name": "state",
              "inputValue": estado
            },
            {
              "name": "postCode",
              "inputValue": cp
            },
            {
              "name": "description",
              "inputValue": referencia
            },
            {
              "name": "expectedTime",
              "inputValue": "2019-04-12T14:00:00.993Z"
            },
            {
              "name": "customerMobileNo",
              "inputValue": formulario.cliente.celular
            },
            {
              "name": "customerPhoneNumber",
              "inputValue": formulario.cliente.telefono
            },
            {
              "name": "customerEmail",
              "inputValue": formulario.cliente.correo
            },
            {
              "name": "totalAmount",
              "inputValue": formulario.orden.total
            },
            {
              "name": "formOfPayment",
              "inputValue": formulario.orden.met_pago
            },
            {
              "name": "prepareChangeFor",
              "inputValue": formulario.orden.cambio
            },
            {
              "name": "specialInstructions",
              "inputValue": formulario.orden.comentarios
            },
            {
              "name": "orderEntryMethod",
              "inputValue": formulario.orden.orderEntryMethod
            },
            {
              "name": "priority",
              "inputValue": "1"
            },
            {
              "name": "skillSets",
              "inputValueList": ["Food Delivery"]
            }

          ]
        };

      console.log($scope.data);

      $('.loader').show();

      $http({
        method: "POST",
        url: "https://dista-dish-sandbox.appspot.com/_ah/api/jobsApi/v1/dish/create/job",
        headers: {
          'CLIENT_ID': '5194283485233152',
          'Content-Type': 'application/json',
          'SECRET_ID': 'K1%>Xg5I-0]g]_W.u97t',
          'USER_ID': '4960327154270208'
        },
        data: $scope.data

      }).then(function mySuccess(response) {
          console.log(response);
          if (response.data.status == "failure") {
            Swal.fire({
              position: 'top-end',
              type: 'error',
              title: response.data.message,
              showConfirmButton: false,
              timer: 2000
            });
            $('.loader').hide();
          };

          if (response.data.status == "failure") {
            Swal.fire({
              position: 'top-end',
              type: 'success',
              title: "Trabajo creado exitosamente",
              showConfirmButton: false,
              timer: 2000
            });
            $('.loader').hide();
          }

        },
        function myError(response) {
          console.log(response);
          $('.loader').hide();
        });


    };




  }]);
})();