package com.bolsadeideas.spring.boot.backend.apirest.springbootbackendapirest.models.dao;

import com.bolsadeideas.spring.boot.backend.apirest.springbootbackendapirest.models.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IClienteDao extends JpaRepository<Cliente, Long> {



}
